Projet Django : Site de recette

# Create BDD
python manage.py migrate

# Charge les jeux de données (fixtures)
python manage.py loaddata recette/fixtures/initial_data.json

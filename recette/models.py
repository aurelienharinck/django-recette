from django.db import models
from django.contrib.auth.models import User

class Recette(models.Model):
    titre = models.CharField(max_length=120)
    id_type = models.ForeignKey('TypeRecette', on_delete=models.CASCADE)
    difficulte = models.FloatField(default=0.0)
    temps_prep = models.PositiveSmallIntegerField()
    temps_cuisson = models.PositiveSmallIntegerField()
    temps_repos = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.titre

class CommentaireRecette(models.Model):
    id_recette = models.ForeignKey('Recette', on_delete=models.CASCADE)
    commentaire = models.TextField(max_length=500)
    date_publication = models.DateTimeField(auto_now_add=True)
    #user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.commentaire

class NoteRecette(models.Model):
    id_recette = models.ForeignKey('Recette', on_delete=models.CASCADE)
    note = models.FloatField(default=0.0)
    #user = models.ForeignKey(settings.AUTHUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.note

class TypeRecette(models.Model):
    type = models.CharField(max_length=50)

    def __str__(self):
        return self.type

class EtapeRecette(models.Model):
    id_recette = models.ForeignKey('Recette', on_delete=models.CASCADE)
    etape = models.TextField(max_length=500)

    def __str__(self):
        return self.etape

class Ingredient(models.Model):
    nom = models.CharField(max_length=50)

    def __str__(self):
        return self.nom

class IngredientRecette(models.Model):
    id_recette = models.ForeignKey('Recette', on_delete=models.CASCADE)
    id_ingredient = models.ForeignKey('Ingredient', on_delete=models.CASCADE)

class PhotoRecette(models.Model):
    id_recette = models.ForeignKey('Recette', on_delete=models.CASCADE)
    photo = models.FilePathField()

    def __str__(self):
        return self.photo

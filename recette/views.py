from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404

from recette.forms import ConnexionForm, RecetteForm, EtapeForm, IngredientForm, CommentaireRecetteForm, NoteRecetteForm
from recette.models import Recette, Ingredient, IngredientRecette, EtapeRecette


def index(request):
    recettes = Recette.objects.all()
    return render(request, 'home.html', {'recettes': recettes})


def connexion(request):
    error = False

    if request.method == "POST":
        form = ConnexionForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(user=username, password=password)

            if user:
                login(request, user)
            else:
                error = True
            return render(request, 'recette/index.html')
    else:
        form = ConnexionForm()

    return render(request, 'recette/connexion.html', {'form': form})

def new_recette_et1(request):

    form = RecetteForm(request.POST)

    if form.is_valid():

        titre = form.cleaned_data['titre']
        type = form.cleaned_data['id_type']
        difficulte = form.cleaned_data['difficulte']
        temps_prep = form.cleaned_data['temps_prep']
        temps_cuisson = form.cleaned_data['temps_cuisson']
        temps_repos = form.cleaned_data['temps_repos']

        if Recette.objects.filter(titre=titre).exists():
            form = RecetteForm()
            return render(request, 'recette/new_recette_et1.html', {'form': form})

        else:

            Recette(titre=titre,
                    id_type=type,
                    difficulte=difficulte,
                    temps_prep=temps_prep,
                    temps_cuisson=temps_cuisson,
                    temps_repos=temps_repos).save()

            request.session['titre_recette'] = titre
            return HttpResponseRedirect('etape2')
    else:
        form = RecetteForm()
        return render(request, 'recette/new_recette_et1.html', {'form': form})


def new_recette_et2(request):

    form = IngredientForm(request.POST)

    if form.is_valid():

        #Recherche de l'id de la recette
        titre_recette = request.session.get('titre_recette')
        recette = Recette.objects.filter(titre=titre_recette).get()
        #id_recette = recette[0].id

        #Recherche de l'id de l'ingredient
        ingredientValueForm = form.cleaned_data['ingredient']
        ingredient = Ingredient.objects.filter(nom=ingredientValueForm).get()
        #id_ingredient = ingredient[0].id

        IngredientRecette(id_recette=recette,
                          id_ingredient=ingredient).save()

        return HttpResponseRedirect('etape3')
    else:
        form = IngredientForm()
        return render(request, 'recette/new_recette_et2.html', {'form': form})


def new_recette_et3(request):

    form = EtapeForm(request.POST)

    # Recherche de l'id de la recette
    titre_recette = request.session.get('titre_recette')
    recette = Recette.objects.filter(titre=titre_recette).get()
    #id_recette = recette[0].id

    if form.is_valid():
        etape = form.cleaned_data['etape']

        EtapeRecette(id_recette=recette,
                     etape=etape).save()

        return redirect('home')
    else:
        form = EtapeForm()
        return render(request, 'recette/new_recette_et3.html', {'form' : form})


# Create your views here.

def modify_recette(request, id_recette):
    recette = Recette.objects.get(id=id_recette)
    form = RecetteForm(request.POST, instance=recette)
    if form.is_valid():
        form.save()
        return redirect('home')
    else:
        return render(request, 'recette/modify_recette.html', {'form': form, 'recette': recette})

def new_commentaire(request):
    form = CommentaireRecetteForm(request.POST)
    if form.is_valid():
        form.save()
        return redirect('home')
    else:

        return render(request, 'recette/new_commentaire.html', {'form': form})

def new_note(request):
    form = NoteRecetteForm(request.POST)

    if form.is_valid():
        form.save()
        return redirect('home')
    else:
        return render(request, 'recette/new_note.html', {'form': form})

def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})



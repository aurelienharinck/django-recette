from django import forms

from recette.models import Recette,CommentaireRecette,Ingredient, NoteRecette


class ConnexionForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)

class EtapeForm(forms.Form):
    etape = forms.CharField(label="Etapes de préparation", max_length=500)

class IngredientForm(forms.Form):
    ingredient = forms.ModelChoiceField(queryset=Ingredient.objects.all())

class RecetteForm(forms.ModelForm):
    class Meta:
        model = Recette
        fields = [
            'titre',
            'id_type',
            'difficulte',
            'temps_prep',
            'temps_cuisson',
            'temps_repos'
        ]
class CommentaireRecetteForm(forms.ModelForm):
    class Meta:
        model = CommentaireRecette
        fields = [
            'commentaire',
            'id_recette'
        ]
class NoteRecetteForm(forms.ModelForm):
    class Meta:
        model = NoteRecette
        fields = [
            'note',
            'id_recette'
        ]
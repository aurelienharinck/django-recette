"""recette URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from recette import views

urlpatterns = [
    path('', views.index, name='home'),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('signup/', views.signup, name="signup"),
    path('new_recette/etape1', views.new_recette_et1, name='new_recette_et1'),
    path('new_recette/etape2', views.new_recette_et2, name='new_recette_et2'),
    path('new_recette/etape3', views.new_recette_et3, name='new_recette_et3'),

    path('commentaire/', views.new_commentaire, name='new_commentaire'),
    path('note/', views.new_note, name='new_note'),
    path('modify_recette/<int:id_recette>', views.modify_recette, name='modify_recette')
]

from django.contrib import admin
from recette.models import Recette, TypeRecette, EtapeRecette, IngredientRecette, Ingredient, CommentaireRecette, \
    NoteRecette

# Register your models here.

admin.site.register(Recette)
admin.site.register(TypeRecette)
admin.site.register(EtapeRecette)
admin.site.register(IngredientRecette)
admin.site.register(Ingredient)
admin.site.register(CommentaireRecette)
admin.site.register(NoteRecette)
